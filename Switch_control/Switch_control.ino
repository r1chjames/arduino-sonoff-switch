#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>
#include <ArduinoOTA.h>

#define mqtt_server "192.168.0.2"
#define mqtt_user "homeassistant"
#define mqtt_password "2032"
#define mqtt_client "switch_01"

#define switch_topic "switch/control/01"
int gpio13Led = 13;
int gpio12Relay = 12;

WiFiClient espClient;
PubSubClient client(espClient);

void setUpGpio() {
  pinMode(gpio13Led, OUTPUT);
  digitalWrite(gpio13Led, HIGH);  
  pinMode(gpio12Relay, OUTPUT);
  digitalWrite(gpio12Relay, HIGH);
}

void setup() {
  setUpGpio();
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883); 
  client.setCallback(process_message);
  client.subscribe(switch_topic);
  setup_OTA();
}

void setup_OTA() {
  ArduinoOTA.setHostname(mqtt_client);
  ArduinoOTA.onStart([]() {
    Serial.println("Start");
  });

  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });

  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });

  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });

  ArduinoOTA.begin();
  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void setup_wifi() {
  WiFiManager wifiManager;
  wifiManager.autoConnect(mqtt_client);
}

void reconnect() {
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection... ");
    if (client.connect(mqtt_client, mqtt_user, mqtt_password)) {
      Serial.println("Connected");
      client.subscribe(switch_topic);
      Serial.println("Subscribed to topic");
    } else {
      Serial.print("Failed to connect, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void process_message(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  String payloadStr;
  for (int i=0;i<length;i++) {
    payloadStr = payloadStr + (char)payload[i];
  }
  Serial.print(payloadStr);
  Serial.println();
  actuateSwitch(payloadStr);
}

void switchOn() {
    digitalWrite(gpio13Led, LOW);
    digitalWrite(gpio12Relay, HIGH);  
    Serial.println("Switch on");
}

void switchOff() {
    digitalWrite(gpio13Led, HIGH);
    digitalWrite(gpio12Relay, LOW);    
    Serial.println("Switch off");
}

void actuateSwitch(String action) {
  if (action == "on") {
    switchOn();
  } else if (action == "off") {
    switchOff();
  }
}

void loop() {
  ArduinoOTA.handle();

  if (!client.connected()) {
    Serial.print("Not connected to MQTT. State: ");
    Serial.println(client.state());
    reconnect();
  }
  client.loop();

}
